const webpack = require('webpack')
const path = require('path')
const config = require('config')
const { VueLoaderPlugin } = require( 'vue-loader' );
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin');

const paths = {
    src: __dirname + '/src',
    dist: __dirname + '/web',
    public: `/${process.env.PUBLIC_PATH || ''}/`.replace('//', '/'),
}


module.exports = {
    mode: process.env.NODE_ENV,
    context: paths.src,
    entry: {
        app: `${paths.src}/main.js`,
    },
    output: {
        filename: '[name].js?v=[hash]',
        path: paths.dist,
        publicPath: paths.public,
    },
    devtool: 'cheap-source-map',
    resolve: {
        modules: [paths.src, 'node_modules'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
			'@modules': path.resolve( './src/modules' ),
			'@assets': path.resolve( './src/assets' ),
			'@components': path.resolve( './src/components' ),
			'@pages': path.resolve( './src/pages' ),
			'@sections': path.resolve( './src/sections' ),
			'@mixins': path.resolve( './src/mixins' ),
        },
    },
    devServer: {
        contentBase: paths.dist,
        compress: true,
        progress: false,
        noInfo: true,
        https: true,
        hot: true,
        host: 'localhost',
        port: '8443',
        historyApiFallback: {index: paths.public},
    },
    module: {
        rules: [
            {
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					loaders: {
						'scss': 'vue-style-loader!css-loader!sass-loader',
						'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
					}
				}
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				loader: 'file-loader',
				
				options: {
					name: '[name].[ext]',
					path: paths.dist,
        			publicPath: paths.public,
				}
			},
			{
				test: /\.css$/,
				use: [
					'vue-style-loader',
					'css-loader'
				]
			},
			{
				test: /\.(sass|scss)$/,
				use: [
					'vue-style-loader',
					'css-loader',
					'sass-loader'
				]
			}
        ],
    },
    plugins: [
        new webpack.ProgressPlugin(),
		new VueLoaderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            
        }),
        new HtmlWebpackPlugin({
            title: config.get('appTitle'),
            filename: 'index.html',
            template: path.join(process.cwd(), 'public/index.ejs'),
        }),
        new CopyWebpackPlugin([
			{
				from: paths.src+'/assets/',
				to: paths.dist+'/assets/'
			},
			{
				from: paths.src+'/public/favicon.ico',
				to: paths.dist+'/favicon.ico'
			},
		]),
    ],
}
if ( process.env.NODE_ENV === 'production' ) {
	module.exports.devtool = 'source-map';
	module.exports.mode = 'production';
}