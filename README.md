Install
=======

```yarn``` or ```npm i```


Develop
=======

```yarn start``` or ```npm start```

App will be available at ```https://localhost:443/```

Build
=====

```yarn build``` or ```npm run build``` - for production
```yarn build:dev``` or ```npm run build:dev``` - for testing

The build is in the "web" directory


Components structure
--------------------

```common``` - обычные переиспользуемые компоненты

```rare``` - компоненты, которые не переиспользуются

```pages``` - страницы
