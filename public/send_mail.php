<?php
if (!isset($_POST['hash_key'])) {
    http_response_code(404);
    return;
}
if (!isset($_POST['json'])) {
    http_response_code(404);
    return;
}

if ($_POST['hash_key'] != md5($_POST['json'])) {
    http_response_code(403);
    return;
}

make_mail($_POST['json']);

/**
 * @param string $json
 */
function make_mail($json)
{
    $allField = json_decode($json, true);
    $subject = 'Сообщение с сайта';
    $commonFiels = [
        
    ];
    $message = '';
    foreach ($commonFiels as $fieldName => $title) {
        if (isset($allField[$fieldName]) && is_array($allField[$fieldName])) {
            $message .= $title . ": " . implode( ',', $allField[$fieldName] ) . "\n";
        } else if (isset($allField[$fieldName])) {
            $message .= $title . ": " . $allField[$fieldName] . "\n";
        }
    }
    if (isset($allField['data'])) {
        $message .= generate_message_from_data($allField['data']);
    }

    if (isset($allField['subject'])) {
        $subject = $allField['subject'];
    }
    //  info@rucard.net
    if (send_mail('rusoft-portal@rusoft-company.ru', $subject, $message)) {
        http_response_code(200);
        return;
    }
    http_response_code(500);
    return;
}

/**
 * @param array $data
 * @return string
 */
function generate_message_from_data($data = [])
{
    $message = '';
    foreach ($data as $ind => $field) {
        if (isset($field['title']) && isset($field['value'])) {
            $message .= $field['title'] . ": " . json_encode($field['value']) . "\n";
        } else if ( isset($field['value']) ){
            $message .= $field['value'] . "\n";
        }
    }
    $message .= $allField;
    return $message;
}

/**
 * @param string $to
 * @param string $subject
 * @param string $message
 * @return bool
 */
function send_mail($to = '', $subject = '', $message = '')
{
    $headers = 'From: ...' . "\r\n" .
        'Reply-To: ...' . "\r\n" .
        'Content-Type: text/plain; charset=UTF-8' . "\r\n" .
        'Content-Transfer-Encoding: 8bit' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    return mail($to, "=?utf-8?B?" . base64_encode($subject) . "?=", $message, $headers);
}