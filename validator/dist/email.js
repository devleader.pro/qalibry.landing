'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (text) {
    var email = String(text || '');
    var result = false;
    var atIndex = email.indexOf('@');
    if (atIndex > 0) {
        var local = email.substring(0, atIndex);
        var domain = email.substring(atIndex + 1);
        if (domain.length > 0 && domain.indexOf('@') < 0) {
            result = checkLocal(local) && checkDomain(domain);
        }
    }
    return result;
};

var _punycode = require('punycode');

var _punycode2 = _interopRequireDefault(_punycode);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Переведено из валидатора почты hibernate, использующегося на сервере по аннотации @Email
var maxLocalLength = 64;
var localSymbol = '[a-z0-9!#$%&\'*+/=?^_`{|}~\x80-\uFFFF-]';
var localPattern = new RegExp('^' + localSymbol + '+(\\.' + localSymbol + '+)*$', 'i');

var checkLocal = function checkLocal(local) {
    return local.length <= maxLocalLength && local.search(localPattern) === 0;
};

var maxDomainLength = 255;
var domainSymbol = "[a-z0-9!#$%&'*+\/=?^_`{|}~-]";
var domainPatternPart = domainSymbol + '{1,63}(\\.' + domainSymbol + '{1,63})*';
var ipDomainPatternPart = '\[\d{1,3}(?:\.\d{1,3}){3}\]';
var domainPattern = new RegExp('^(?:' + domainPatternPart + '|' + ipDomainPatternPart + ')$', 'i');

var checkDomain = function checkDomain(domain) {
    var encodedDomain = _punycode2.default.toASCII(domain);
    return encodedDomain.length <= maxDomainLength && encodedDomain.search(domainPattern) === 0;
};
//# sourceMappingURL=email.js.map