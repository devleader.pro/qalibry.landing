export default {
    title: {
        ru: `Скидочная программа`,
        en: 'Discount program',
    },
    description: {
        ru:
            'Программа предоставляет возможность устанавливать скидки вашим покупателям. В программе можно поставить общую скидку для всех покупателей, а также установить индивидуальные привилегии.',
        en:
            'Program provides for discounts to be extend to your customers. You can offer a general discount for all customers as well as customer specific benefits.',
    },
    integrationWithCash: {
        title: {
            ru: 'Интеграция с кассовым ПО установленным в магазине;',
            en: 'Integration with point of sale software;',
        },
        oneC: {
            ru: '1C-касса;',
            en: '1C-kassa;',
        },
        hatchM: {
            ru: 'Штрих-М;',
            en: 'Shtrih – M;',
        },
        electronicMoney: {
            ru: 'Электронные деньги - Кассир.',
            en: 'Electronic money – CASHIER.',
        },
    },
    integrationWithInternetShop: {
        title: {
            ru: 'Интеграция с интернет–магазином',
            en: 'Integration with online store',
        },
    },
    separateSoftwarePackage: {
        ru: 'Можно использовать как отдельный програмнный комплекс',
        en: 'The Customer can use it as a separate software package',
    },
    accessFormSlot: {
        ru: 'Отправить заявку',
        en: 'Send a request',
    },
};
