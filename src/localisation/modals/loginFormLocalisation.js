export default {
    title: {
        ru: 'Личный кабинет',
        en: 'Personal account',
    },
    userName: {
        ru: 'Логин',
        en: 'Login',
    },
    password: {
        ru: 'Пароль',
        en: 'Password',
    },
    login: {
        ru: 'Войти',
        en: 'Sign in',
    },
    registration: {
        ru: 'Зарегистрироваться',
        en: 'Register',
    },
};
