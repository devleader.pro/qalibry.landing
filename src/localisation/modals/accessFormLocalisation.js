export default {
    name: {
        ru: `Имя`,
        en: 'Name',
    },
    email: {
        ru: `E-mail`,
        en: 'E-mail',
    },
    phone: {
        ru: `Телефон`,
        en: 'Phone number',
    },
    message: {
        ru: `Сообщение`,
        en: 'Message',
    },
    agreement: {
        ru: `Ознакомлен и согласен с политикой обработки персональных данных`,
        en: 'I have read and agree with the Personal Data Processing Policy',
    },
};
