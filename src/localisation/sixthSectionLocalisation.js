export default {
    title: {
        ru: 'Программы',
        en: 'Programs',
    },
    description: {
        ru:
            'В программный аппаратный комплекс входит 7 програм лояльности, которые вы можете использовать <br/> для достижения своих бизнес-задач. Можно использовать сразу несколько программ лояльности.',
        en:
            'The hardware and software system includes 7 types of loyalty programs that you can freely use. You can simultaneously use several types of loyalty programs.',
    },
    discountProgram: {
        ru: 'Скидочная <br /> программа',
        en: 'Discount <br /> program ',
    },
    giftProgram: {
        ru: 'Подарочная <br /> программа',
        en: 'Gift <br /> program',
    },
    bonusProgram: {
        ru: 'Бонусная <br /> программа',
        en: 'Bonus <br /> program',
    },
    virtualCertificate: {
        ru: 'Виртуальный <br /> подарочный сертификат',
        en: 'Electronic <br /> gift certificate',
    },
    internetShopProgram: {
        ru: 'Интернет-магазины',
        en: 'Online stores',
    },
    coalitionProgram: {
        ru: 'Совместная <br /> коалиционная программа',
        en: 'Merged <br /> loyalty program',
    },
    multiLevelProgram: {
        ru: 'Многоуровневая <br /> программа',
        en: 'Multi-level <br /> program',
    },
    individualProgram: {
        ru: 'Индивидуальная <br /> программа',
        en: 'Customized <br /> program',
    },
};
