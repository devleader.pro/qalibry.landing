export default {
    technicalHelp: {
        ru: 'Техническая поддержка',
        en: 'Customer support',
    },
    helpSchedule: {
        ru: 'Ежедневно с 10:00 до 19:00',
        en: 'Monday to Sunday from 10:00 AM to 07:00 PM',
    },
    connect: {
        ru: 'Подключиться',
        en: 'Join us',
    },
    login: {
        ru: 'Войти',
        en: 'Sign in',
    },
    salesIncrement: {
        ru: 'Эффективное повышение продаж',
        en: 'Effective sales increase',
    },
    loyaltyProgram: {
        ru: 'Программа <br/> лояльности',
        en: 'Loyalty <br/> program',
    },
    withoutInvestment: {
        ru: 'Внедрение без вложений',
        en: 'Deployment without investments',
    },
    unlimitedTestPeriod: {
        ru: 'Бессрочный тестовый период',
        en: 'Unlimited test period',
    },
    fastSettings: {
        ru: 'Быстрая настройка и внедрение',
        en: 'Quick setup and deployment',
    },
    simpleControl: {
        ru: 'Простое и легкое управление',
        en: 'Simple and easy management',
    },
    comfortableAnalytics: {
        ru: 'Удобная аналитика и отчетность',
        en: 'User-friendly analytics and reporting',
    },
    getDemo: {
        ru: 'Получить Демо-доступ',
        en: 'Get Demo Access',
    },
};
