export default {
    joining: {
        ru: 'Подключение',
        en: 'Joining',
    },
    benefits: {
        ru: 'Преимущества',
        en: 'Benefits',
    },
    functionality: {
        ru: 'Функционал',
        en: 'Functionality',
    },
    programs: {
        ru: 'Программы',
        en: 'Programs',
    },
    rates: {
        ru: 'Тарифы',
        en: 'Schedule of rates',
    },
    feedback: {
        ru: 'Отзывы',
        en: 'Feedback',
    },
};
