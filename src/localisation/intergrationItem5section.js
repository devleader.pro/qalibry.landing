export default {
    integrationPO: {
        title: {
            ru: 'Интеграция с кассовым ПО установленным в магазине',
            en: 'Integration with point of sale software',
        },
        item1: {
            ru: '1С-касса',
            en: '1C-kassa',
        },
        item2: {
            ru: 'R-keeper',
            en: 'R-keeper',
        },
        item3: {
            ru: 'Штрих-М',
            en: 'Shtrih – M',
        },
        item4: {
            ru: 'liko',
            en: 'Iiko',
        },
        item5: {
            ru: 'Электронные деньги - Кассир',
            en: 'Electronic money – CASHIER',
        },
    },
    integrationKassa: {
        title: {
            ru: 'Интеграция с онлайн-кассой согласно 54 –ФЗ',
            en: 'Integration with online cashier according to Federal law No. 54 – FZ',
        },
        item1: {
            ru: 'Модуль касса',
            en: 'ModulKassa',
        },
        item2: {
            ru: 'Эвотор',
            en: 'Evotor',
        },
        item3: {
            ru: 'Атол',
            en: 'Atol',
        },
        item4: {
            ru: 'Штрих-М',
            en: 'Shtrih – M',
        },
        item5: {
            ru: 'Viki mini',
            en: 'Viki mini',
        },
    },
    integrationInternetShop: {
        title: {
            ru: 'Интеграция с интернет–магазином',
            en: 'Integration with online store',
        },
        item1: {
            ru: '1С-Битрикс',
            en: '1C – Bitrix',
        },
        item2: {
            ru: 'Joomla',
            en: 'Joomla',
        },
        item3: {
            ru: 'NetCat',
            en: 'NetCat',
        },
        item4: {
            ru: 'AMIRO.CMS',
            en: 'AMIRO.CMS',
        },
        item5: {
            ru: 'WordPress',
            en: 'WordPress',
        },
        item6: {
            ru: 'CS-Cart',
            en: 'CS-Cart',
        },
    },
    advantages: {
        ru:
            'Возможна и персональная интеграция кассового ПО для компаний, у которых планируемый оборот по программе лояльности может достигать 5 млн. руб.',
        en:
            'We offer personal integration of the point of sale software for companies with the intended turnover under the loyalty program of 5 million rubles.',
    },
    advantagesMore: {
        ru: 'Можно использовать как отдельный програмнный комплекс',
        en: 'The Customer can use it as a separate software package',
    },
};
