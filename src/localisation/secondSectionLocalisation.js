export default {
    salesIncrement: {
        ru: 'Увеличение продаж',
        en: 'Sales increase',
    },
    secondSectionDescription: {
        ru: `Программа предоставляет комплекс инструментов для привлечения новых и удержания действующих клиентов.
            Использование программы лояльности увеличивает количество повторных покупок,
            средний чек, что соответственно отражается на прибыли компании.`,
        en:
            'The program provides a set of tools to acquire new customers and retain the existing ones. Loyalty program encourages customers to purchase more, to increase average check amount, affecting the company profit.',
    },
    before: {
        ru: 'До',
        en: 'Before',
    },
    after: {
        ru: 'После',
        en: 'After',
    },
    averageSum: {
        ru: 'Средний чек<br/>в рублях',
        en: 'Average check amount<br/>in rubles',
    },
    repeatableShopCount: {
        ru: 'Кол-во повторных покупок в месяц',
        en: 'Number of repeat purchases per month',
    },
    averageShopCount: {
        ru: 'Среднее кол-во товаров в одном чеке за месяц',
        en: 'Average number of goods under one check per month',
    },
};
