export default {
    title: {
        ru: 'Тарифы',
        en: 'Schedule of rates',
    },
    standart: {
        title: {
            ru: 'СТАНДАРТ',
            en: 'Standard',
        },
        description: {
            ru: 'Для ИП и небольших компаний',
            en: 'Rates for sole entrepreneurs and SMEs',
        },
        descriptionPercent: {
            ru: 'от оборота',
            en: 'Rates based on the turnover',
        },
        descriptionItem1: {
            ru: 'Абонентская плата 2\xA0500\xA0₽<br />при обороте  <\xA030\xA0000\xA0₽ ',
            en: 'Service fee is 2\xA0500\xA0₽ if the turnover is less than 30\xA0000\xA0₽',
        },
        descriptionItem2: {
            ru: 'Бессрочный тестовый доступ',
            en: 'Unlimited test access',
        },
        descriptionItem3: {
            ru: '7 программ лояльности',
            en: '7 types of loyalty programs',
        },
        descriptionItem4: {
            ru: 'Выезд технического специалиста 4\xA0000\xA0₽ внутри МКАД',
            en: `Price of Technician’s visit from 4\xA0000\xA0₽ inside MKAD (Moscow automobile ring road)`,
        },
    },
    vip: {
        title: {
            ru: 'VIP',
            en: 'VIP',
        },
        description: {
            ru: 'Для крупных компаний и торговых сетей',
            en: 'For major companies and retail chains',
        },
        descriptionPercent: {
            ru: 'индивидуальные финансовые условия',
            en: 'Customized financial terms',
        },
        descriptionItem1: {
            ru: 'Абонентская плата отсутствует',
            en: 'No service fee',
        },
        descriptionItem2: {
            ru: 'Бессрочный тестовый доступ',
            en: 'Unlimited test access',
        },
        descriptionItem3: {
            ru: '7 программ лояльности',
            en: '7 types of loyalty programs',
        },
        descriptionItem4: {
            ru: 'Бесплатный выезд технического специалиста раз в квартал',
            en: 'Free of charge technician’s visit once in a quarter',
        },
        descriptionItem5: {
            ru: 'Маркетинговая поддержка',
            en: 'Marketing support',
        },
        descriptionItem6: {
            ru: 'Разработка индивидуальных программ лояльности',
            en: 'Development of customized loyalty programs',
        },
    },
    plus: {
        title: {
            ru: 'Стандарт+',
            en: 'Standard+',
        },
        description: {
            ru: 'Для средних компаний',
            en: 'For medium-sized companies',
        },
        descriptionPercent: {
            ru: 'от оборота',
            en: 'Rates based on the turnover',
        },
        descriptionItem1: {
            ru: 'Абонентская плата 2\xA0500\xA0₽<br />при обороте  <\xA050\xA0000\xA0₽',
            en: 'Service fee is 2\xA0500\xA0₽ if the turnover is less than 50\xA0000\xA0₽',
        },
        descriptionItem2: {
            ru: 'Бессрочный тестовый доступ',
            en: 'Unlimited test access',
        },
        descriptionItem3: {
            ru: '7 программ лояльности',
            en: '7 types of loyalty programs',
        },
        descriptionItem4: {
            ru: 'Выезд технического специалиста 4\xA0000\xA0₽ внутри МКАД',
            en: 'Price of Technician’s visit from 4\xA0000\xA0₽ inside MKAD (Moscow automobile ring road)',
        },
    },
    premium: {
        title: {
            ru: 'Премиум',
            en: 'Premium',
        },
        description: {
            ru: 'Для средних и больших компаний',
            en: 'For medium and large companies',
        },
        descriptionPercent: {
            ru: 'от оборота',
            en: 'Rates based on the turnover',
        },
        descriptionItem1: {
            ru: 'Абонентская плата отсутствует',
            en: 'No service fee',
        },
        descriptionItem2: {
            ru: 'Бессрочный тестовый доступ',
            en: '',
        },
        descriptionItem3: {
            ru: '7 программ лояльности',
            en: 'Unlimited test access',
        },
        descriptionItem4: {
            ru: '1 бесплатный выезд технического специалиста',
            en: 'One free of charge technician’s visit',
        },
        descriptionItem5: {
            ru: 'Маркетинговая поддержка',
            en: 'Marketing support',
        },
    },
    textFooter: {
        ru:
            'Подключение к программе лояльности бесплатно. Для начала использования никаких оплат не потребуется. Компания оплачивает услуги сервиса по использованию программы лояльности в конце отчетного периода (месяца) <br /> по выставленному счету согласно условиям договора. Если вам необходима программа с индивидуальными настройками, то вы можете оставить заявку, и менеджер свяжется с вами для уточнения всех вопросов. <br /> Стоимость выезда одного технического специалиста для настройки или обучения в точку продажи в пределах МКАД — 4\xA0000\xA0₽. За МКАД +\xA0300\xA0₽ за 1\xA0км.',
        en:
            'You can join the loyalty program at no charge. To start using the program no charges are required as well. The company pays for using the loyalty program at the end of the reporting period (month) upon the invoice issued in accordance with the terms and conditions of the Agreement. If you need an individual program, you can leave a request, and our manager will contact you at the shortest time possible. Price of the technician’s visit to the point of sale within MKAD (Moscow Ring Road) is 4\xA0000\xA0₽, beyond MKAD )Moscow Ring Road) is plus 300\xA0₽ for 1\xA0km.',
    },
};
