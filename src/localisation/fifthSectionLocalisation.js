export default {
    functionsTitle: {
        ru: 'Функционал',
        en: 'Functionality',
    },
    intergation: {
        ru: 'Интеграция',
        en: 'Integration',
    },
    settings: {
        ru: 'Настройки',
        en: 'Settings',
    },
    communication: {
        ru: 'Коммуникация',
        en: 'Networking',
    },
    analytics: {
        ru: 'Аналитика',
        en: 'Analytics',
    },
    security: {
        ru: 'Безопаность',
        en: 'Safety',
    },
    comfort: {
        ru: 'Удобство',
        en: 'Easy-to-use',
    },
};
