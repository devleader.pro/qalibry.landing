export default {
    connect: {
        ru: 'Подключение',
        en: 'Joining the program',
    },
    thirdSectionDescription: {
        ru: `После легкой регистрации вашей компании или ИП вам будет доступен демо-доступ
        к платформе. Платформа подходит абсолютно к любому виду деятельности.
        Свяжитесь с нами, и мы предложим оптимальное решение для ваших бизнес-задач.`,
        en:
            'Once a company or a sole entrepreneur is registered with the service, you will gain Demo Access to the platform. The platform can match any kind of activity. Contact us and we will offer the best solution for your business.',
    },
    registration: {
        ru: 'Зарегистрироваться',
        en: 'Register',
    },
    step1: {
        ru: 'Шаг 1',
        en: 'Step 1',
    },
    step2: {
        ru: 'Шаг 2',
        en: 'Step 2',
    },
    step3: {
        ru: 'Шаг 3',
        en: 'Step 3',
    },
    step4: {
        ru: 'Шаг 4',
        en: 'Step 4',
    },
    stepOneDescription: {
        ru: 'Оставить заявку на подключение и зарегистрироваться',
        en: 'Leave a request if you wish to get registered',
    },
    stepTwoDescription: {
        ru: 'Выбрать тариф',
        en: 'Choose a tariff rate',
    },
    stepThreeDescription: {
        ru: 'Подключить оборудование',
        en: 'Connect equipment',
    },
    stepFourDescription: {
        ru: 'Подписать договор',
        en: 'Sign agreement',
    },
};
