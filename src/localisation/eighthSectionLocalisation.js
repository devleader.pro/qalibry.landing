export default {
    title: {
        ru: `Отзывы`,
        en: 'Feedback',
    },
    comments: {
        first: {
            firstPart: {
                ru: `При внедрении программы лояльности нам было важно иметь гибкую аналитику продаж и
                        возможность выгрузки в Excel, так как это дает возможность построить
                        любые отчеты по маркетингу и продажам.`,
                en:
                    'When deploying a loyalty program, it was important to have flexible sales analytics and the ability to upload to Excel, as all that enable us to make any types of marketing and sales reports.',
            },
            secondPart: {
                ru: `В программе реализована очень удобная выгрузка отчета и аналитики,
                        а также быстрая и простая настройка любой программы. Когда мы стали
                        тестировать программу лояльности «Qalibry», у нас не осталось сомнений,
                        что будем использовать именно её. Менеджер все подробно объяснил и
                        предоставил всю информацию по использованию и подключению программы.
                        В течение двух дней  подключили, протестировали и начали работать.`,
                en:
                    'Program user can easily upload reports and analytics as well as quickly configure any program. When we began to test Qalibry loyalty program, we had no doubt that we would use this particular one from now on. The manager explained everything in detail and provided us with all information on how to join and use the program. Within two days we joined, tested and started using it.',
            },
            author: {
                ru: 'Курочкин Иван Николаевич',
                en: 'Kurochkin Ivan Nikolaevich',
            },
            position: {
                ru: 'Директор по маркетингу',
                en: 'Chief Marketing officer',
            },
        },
    },
};
