export default {
    yourLoyaltyProgram: {
        ru: `Ваша программа лояльности`,
        en: 'loyalty program',
    },
    contacts: {
        ru: `Контакты`,
        en: 'Contacts',
    },
    technicalHelp: {
        ru: `Круглосуточная техническая поддержка`,
        en: '24/7 Customer Support',
    },
    connectionSchedule: {
        ru: `Подключение программ лояльности ежедневно с 10:00 до 19:00`,
        en: 'You can join a loyalty programs seven days a week from 10:00 AM to 07:00 PM',
    },
    subscribe: {
        ru: `Подписаться`,
        en: 'Subscribe',
    },
    loyaltyProgram: {
        ru: `Программа лояльности`,
        en: 'Loyalty program',
    },
    discountProgram: {
        ru: `Скидочная программа`,
        en: 'Discount program',
    },
    giftProgram: {
        ru: `Подарочная программа`,
        en: 'Gift Program',
    },
    bonusProgram: {
        ru: `Бонусная программа`,
        en: 'Bonus program',
    },
    virtualCertificate: {
        ru: `Виртуальный подарочный сертификат`,
        en: 'Electronic gift certificate',
    },
    internetShopProgram: {
        ru: `Программа для интернет-магазинов`,
        en: 'Program for online stores',
    },
    coalitionProgram: {
        ru: `Совместная коалиционная программа`,
        en: 'Merged loyalty program',
    },
    multiLevelProgram: {
        ru: `Многоуровневая программа`,
        en: 'Multi-level program',
    },
    documentation: {
        ru: `Документация`,
        en: 'Documents',
    },
    integration: {
        ru: `Интеграция`,
        en: 'Integration',
    },
    contractExample: {
        ru: `Образец договора`,
        en: 'Sample contract',
    },
    requisites: {
        ru: `Реквизиты`,
        en: 'Contact information',
    },
    conditionsOfUsage: {
        ru: `Условия использования`,
        en: 'Terms of Use',
    },
};
