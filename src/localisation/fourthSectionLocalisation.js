export default {
    featuresTitle: {
        ru: 'Преимущества',
        en: 'Benefits',
    },
    featureItemsLeft: [
        {
            title: {
                ru: 'Внедрение без вложений',
                en: 'Deployment without investments',
            },
            description_first: {
                ru:
                    'Данный программный комплекс подключается бесплатно и не требует каких-либо вложений и предоплат. Оплата за использование программного комплекса происходит после отчетного периода 30 (тридцати) календарных дней.',
                en:
                    'You can install the software package free of charge. First payment for using the software package shall be performed upon the expiry of the reporting period of 30 (thirty) calendar days.',
            },
            description_second: {
                ru: 'После прохождения отчетного периода выставляется счет на оплату согласно условиям договора.',
                en:
                    'Once the reporting period terminates, the Customer is invoiced in accordance with the terms of the Agreement.',
            },
        },
        {
            title: {
                ru: 'Быстрое внедрение и настройка',
                en: 'Quick deployment and configuration',
            },
            description_first: {
                ru:
                    'Данный программный комплекс подключается бесплатно и не требует каких-либо вложений и предоплат. Оплата за использование программного комплекса происходит после отчетного периода 30 (тридцати) календарных дней.',
                en:
                    'You can install the software package free of charge. First payment for using the software package shall be performed upon the expiry of the reporting period of 30 (thirty) calendar days.',
            },
            description_second: {
                ru: 'После прохождения отчетного периода выставляется счет на оплату согласно условиям договора.',
                en:
                    'Once the reporting period terminates, the Customer is invoiced in accordance with the terms of the Agreement.',
            },
        },
        {
            title: {
                ru: 'Аналитика и отчетность',
                en: 'Analytics and settings',
            },
            description_first: {
                ru:
                    'Данный программный комплекс подключается бесплатно и не требует каких-либо вложений и предоплат. Оплата за использование программного комплекса происходит после отчетного периода 30 (тридцати) календарных дней.',
                en:
                    'You can install the software package free of charge. First payment for using the software package shall be performed upon the expiry of the reporting period of 30 (thirty) calendar days.',
            },
            description_second: {
                ru: 'После прохождения отчетного периода выставляется счет на оплату согласно условиям договора.',
                en:
                    'Once the reporting period terminates, the Customer is invoiced in accordance with the terms of the Agreement.',
            },
        },
    ],
    featureItemsRight: [
        {
            title: {
                ru: 'Бессрочный тестовый доступ и бесплатный период использования до 25 календарных дней',
                en: 'Unlimited test access and free trial period up to 25 calendar days',
            },
            description_first: {
                ru:
                    'Данный программный комплекс подключается бесплатно и не требует каких-либо вложений и предоплат. Оплата за использование программного комплекса происходит после отчетного периода 30 (тридцати) календарных дней.',
                en:
                    'You can install the software package free of charge. First payment for using the software package shall be performed upon the expiry of the reporting period of 30 (thirty) calendar days.',
            },
            description_second: {
                ru: 'После прохождения отчетного периода выставляется счет на оплату согласно условиям договора.',
                en:
                    'Once the reporting period terminates, the Customer is invoiced in accordance with the terms of the Agreement.',
            },
        },
        {
            title: {
                ru: 'Простое и легкое управление',
                en: 'Simple and easy management',
            },
            description_first: {
                ru:
                    'Данный программный комплекс подключается бесплатно и не требует каких-либо вложений и предоплат. Оплата за использование программного комплекса происходит после отчетного периода 30 (тридцати) календарных дней.',
                en:
                    'You can install the software package free of charge. First payment for using the software package shall be performed upon the expiry of the reporting period of 30 (thirty) calendar days.',
            },
            description_second: {
                ru: 'После прохождения отчетного периода выставляется счет на оплату согласно условиям договора.',
                en:
                    'Once the reporting period terminates, the Customer is invoiced in accordance with the terms of the Agreement.',
            },
        },
        {
            title: {
                ru: 'Техническая и административная поддержка',
                en: 'Technical and administrative support',
            },
            description_first: {
                ru:
                    'Данный программный комплекс подключается бесплатно и не требует каких-либо вложений и предоплат. Оплата за использование программного комплекса происходит после отчетного периода 30 (тридцати) календарных дней.',
                en:
                    'You can install the software package free of charge. First payment for using the software package shall be performed upon the expiry of the reporting period of 30 (thirty) calendar days.',
            },
            description_second: {
                ru: 'После прохождения отчетного периода выставляется счет на оплату согласно условиям договора.',
                en:
                    'Once the reporting period terminates, the Customer is invoiced in accordance with the terms of the Agreement.',
            },
        },
    ],
};
