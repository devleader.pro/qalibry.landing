import AppHome from './pages/Home/Home.vue';

export default {
    created: function() {
        let langs = ['ru', 'en'];
        if (!window.localStorage.lang || langs.indexOf(window.localStorage.lang.substr(0, 2).toLowerCase()) === -1) {
            let language = window.navigator ? window.navigator.language : 'ru';

            language = language.substr(0, 2).toLowerCase();
            this.$root.lang = language;
        } else {
            this.$root.lang = window.localStorage.lang;
        }

        //реализовать кастомизацию меню при скролле
        window.onscroll = e => {
            // console.log(e)
        };
    },
    mixins: [],
    components: {
        AppHome,
    },
    data: () => ({}),
    watch: {
        $lang(newLangValue) {
            localStorage.lang = newLangValue;
        },
    },
};
