//import Plugins
import "babel-polyfill";
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import { HTTP as axios } from './axios/axios';
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
import en from 'vuejs-datepicker/dist/locale/translations/en.js'
import fa from 'vuejs-datepicker/dist/locale/translations/fa.js'
import ru from 'vuejs-datepicker/dist/locale/translations/ru.js'
import ar from 'vuejs-datepicker/dist/locale/translations/ar.js'
const VueInputMask = require('vue-inputmask').default
import VueFlatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';
Vue.use(VueInputMask);

Vue.component('v-select', vSelect);
Vue.prototype.$http = axios;

Vue.use( Vuex );
Vue.use( VueRouter );
Vue.config.debug = true;
Vue.config.devTools = true;
Vue.use(VueFlatPickr);


import App from './App.vue';
Vue.component( 'App', App );
import Home from './pages/Home/Home.vue';
import IntergrationItem from './sections/FifthSection/RouterItems/IntergrationItem.vue';
import AnalyticsItem from './sections/FifthSection/RouterItems/AnalyticsItem.vue';
import CommunicationItem from './sections/FifthSection/RouterItems/CommunicationItem.vue';
import ComfortItem from './sections/FifthSection/RouterItems/ComfortItem.vue';
import SecurityItem from './sections/FifthSection/RouterItems/SecurityItem.vue';
import SettingsItem from './sections/FifthSection/RouterItems/SettingsItem.vue';
Vue.component( 'Home', Home );

//Routers
const router = new VueRouter( {
	mode: 'history',
	routes: [
        {
            path: '/',
            name: 'IntergrationItem',
            component: IntergrationItem
        },
        {
            path: '/integration',
            name: 'IntergrationItem',
            component: IntergrationItem
        },
        {
            path: '/analytics',
            name: 'AnalyticsItem',
            component: AnalyticsItem
        },
        {
            path: '/communication',
            name: 'CommunicationItem',
            component: CommunicationItem
        },
        {
            path: '/comfort',
            name: 'ComfortItem',
            component: ComfortItem
        },
        {
            path: '/security',
            name: 'SecurityItem',
            component: SecurityItem
        },
        {
            path: '/settings',
            name: 'SettingsItem',
            component: SettingsItem
        },
	]
});

//Define rtl/ltr variable
Object.defineProperty(Vue.prototype, '$lang', {
    get: function () {
        return this.$root.lang;
    },
    set: function(lang) {
        this.$root.lang = lang;
    }
});

//Define vuex store
const store = new Vuex.Store( {
	state: {
		title: 'Qalibry'
	},
	mutations: {
		rtChangeTitle( state, value ) {
			state.title = value;
			document.title = ( state.title ? state.title + ' - ' : '' ) + 'Qalibry.Com';
		}
	}
} );

// Create instance of main component
    new Vue( {
	router,
	store,
	render: createElement => createElement( App ),
    beforeMount() {
	    if (window.localStorage.lang === 'fa' || window.localStorage.lang === 'ar') {
	        this.rtl = true;
        }
    },
	data() {
		return {
			rtl: false,
			lang: 'ru',
			modals: {
				openned: false,
				component: '',
				body: {},
			},
			calendarLangs:{
				en: en,
				ru: ru,
				fa: fa,
				ar: ar,
			},
		};
	},
	
	methods: {
		openModal(name, requestFunc, body){
			this.modals = {
				openned: true,
				component: name,
				body: body ? body : {},
                requestFunc: requestFunc
			}
		}
	},
	
	
} ).$mount( '#app' );
