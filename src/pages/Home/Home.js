import FirstSection from "@sections/FirstSection.vue"
import SecondSection from "@sections/SecondSection.vue"
import ThirdSection from "@sections/ThirdSection.vue"
import FourthSection from "@sections/FourthSection/FourthSection.vue"
import FifthSection from "@sections/FifthSection/FifthSection.vue"
import SixthSection from "@sections/SixthSection.vue"
import SeventhSection  from "@sections/SeventhSection.vue"
import EighthSection from "@sections/EighthSection.vue"
import FooterSection  from "@sections/FooterSection.vue"
import ModalWrapper from "@sections/Modals/ModalWrapper.vue"
import DiscountProgramm from "@sections/Modals/DiscountProgramm.vue"


export default {
    components: {
        FirstSection,
        SecondSection,
        ThirdSection,
        FourthSection,
        FifthSection,
        SixthSection,
        SeventhSection,
        EighthSection,
        FooterSection,
        ModalWrapper,
        // DiscountProgramm
    },
    data: ()=>({
        modalIsActive: false,
        currentModal: '',
    }),
    methods: {
        openModal(name) {
            this.modalIsActive = true;
            this.currentModal = name;
            document.querySelector('html').style.overflow = 'hidden';
        },
        onSwitchPopup(name) {
            this.currentModal = name;
        },
        closeModal() {
            this.currentModal = '';
            setTimeout(()=>{
                this.modalIsActive = false;
                document.querySelector('html').style.overflow = 'auto';
            }, 300);

        }
    }
};
